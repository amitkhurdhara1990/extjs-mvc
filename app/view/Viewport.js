Ext.define('ExtJSMVC.view.Viewport', {
    extend: 'Ext.container.Viewport',


    initComponent: function() {
	var me = this;

	Ext.applyIf(me, {
	    items: [
	    {
		xtype: 'toolbar',
		items: [
		{
		    xtype: 'button',
		    itemId:'view1',
		    text: 'Launch View1 Instance'
		},
				{
		    xtype: 'button',
		    itemId:'chart1',
		    text: 'Launch Chart1 Instance'
		}
		]
	    }
	    ]
	});

	me.callParent(arguments);
    }
});